<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_no');
            $table->integer('no_of_box')->nullable();
            $table->float('weight')->nullable();
            $table->foreignId('drop_off_id')->constrained()->onDelete('cascade')->nullable();
            $table->integer('tax')->nullable();
            $table->integer('grand_total')->nullable();
            $table->foreignId('supplier_id')->constrained('clients')->onDelete('cascade');
            $table->foreignId('customer_id')->constrained('clients')->onDelete('cascade');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
};
