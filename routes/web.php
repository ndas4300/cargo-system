<?php

use App\Models\Invoice;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ParcelController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\DropOffController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CompanySettingController;
use App\Http\Controllers\ReportController;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');


Route::group(['middleware' => ['auth']], function() {

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');


    Route::resource('status',StatusController::class)->except('create','edit','show');
    Route::resource('dropOff',DropOffController::class)->except('create','edit','show');
    Route::resource('client',ClientController::class)->except('create','edit','show');
    Route::resource('product',ProductController::class)->except('create','edit','show');
    Route::resource('invoice',InvoiceController::class);
    Route::post('get-client-data',[InvoiceController::class,'getClient'])->name('invoice.getClient');
    // Route::resource('parcel',ParcelController::class);

    // Report
    Route::get('report/product',[ReportController::class,'product'])->name('report.product');
    Route::get('report/invoice',[ReportController::class,'invoice'])->name('report.invoice');
    // Report





    Route::resource('roles', RoleController::class);
    Route::resource('user', UserController::class);
    Route::get('settings/company-settings', [CompanySettingController::class, 'editCompanySetting'])->name('company.edit');
    Route::post('settings/company-setting', [CompanySettingController::class, 'updateCompanySetting'])->name('company.update');
});

Route::get('migrate',function(){
    Artisan::call('migrate');
    dd('done');
});

// Route::get('migrate-fresh',function(){
//     Artisan::call('migrate:fresh --seed');
// });

Route::get('optimize',function(){
    Artisan::call('optimize');
    Artisan::call('optimize:clear');
    dd('done');
});

Route::get('seed',function(){
    Artisan::call('db:seed', [
        '--class' => CountrySeeder::class
    ]);
});

Route::get('rollback',function(){
    Artisan::call('migrate:rollback --step=3');
    dd('done');
});

Route::get('storage-link',function(){
    Artisan::call('storage:link');
    dd('done');
});

// Route::get('drop',function(){
//     Schema::dropIfExists('contacts');
//     dd('done');
// });

require __DIR__.'/auth.php';
