<div class="row">
    <div class="col-6">
        <div class="form-group{{ $errors->has('client_id') ? ' has-error' : '' }}">
            {{ Form::label('client_id', 'Client', ['class' => 'col-sm-9 control-label']) }}
            <div class="col-sm-9">
                {{ Form::select('client_id', $client, null, ['id' => 'client_id','placeholder' => 'Select client', 'class' => 'form-control', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('client_id') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('destination_info') ? ' has-error' : '' }} mb-3">
            {{ Form::label('destination_info', 'Destination Info', ['class' => 'col-sm-9 control-label', 'for' => 'destination_info']) }}
            <div class="col-sm-9">
                {{ Form::text('destination_info', null, ['class' => 'form-control', 'placeholder' => 'Enter destination info', 'id' => 'destination_info']) }}
                <small class="text-danger">{{ $errors->first(' destination_info') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('appx_weight') ? ' has-error' : '' }}">
            {{ Form::label('appx_weight', 'Approximate Weight', ['class' => 'col-sm-9 control-label']) }}
            <div class="col-sm-9">
                {{ Form::number('appx_weight', null, ['class' => 'form-control', 'placeholder' => 'Enter approximate weight', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('appx_weight') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('box') ? ' has-error' : '' }} mb-3">
            {{ Form::label('box', 'Number of Box', ['class' => 'col-sm-9 control-label', 'for' => 'box']) }}
            <div class="col-sm-9">
                {{ Form::number('box', null, ['class' => 'form-control', 'placeholder' => 'Enter number of box', 'id' => 'box', 'required']) }}
                <small class="text-danger">{{ $errors->first('box') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('drop_off_id') ? ' has-error' : '' }}">
            {{ Form::label('drop_off_id', 'Drop Off Location', ['class' => 'col-sm-9 control-label']) }}
            <div class="col-sm-9">
                {{ Form::select('drop_off_id', $drop_off, null, ['id' => 'drop_off_id','placeholder' => 'Select drop off location', 'class' => 'form-control select2', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('drop_off_id') }}</small>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
            {{ Form::label('status_id', 'Status', ['class' => 'col-sm-9 control-label']) }}
            <div class="col-sm-9">
                {{ Form::select('status_id', $status, null, ['id' => 'status_id', 'placeholder' => 'Select status','class' => 'form-control', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('status_id') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
            {{ Form::label('comment', 'Comment', ['class' => 'col-sm-9 control-label']) }}
            <div class="col-sm-9">
                {{ Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Enter comment','rows'=>'9', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('comment') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
            {{ Form::label('date', 'Date', ['class' => 'col-sm-9 control-label']) }}
            <div class="col-sm-9">
                {{ Form::date('date', null, ['class' => 'form-control', 'required' => 'required']) }}
                <small class="text-danger">{{ $errors->first('date') }}</small>
            </div>
        </div>
    </div>
</div>
