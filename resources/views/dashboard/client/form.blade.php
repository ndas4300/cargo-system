<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} mb-3">
    {{ Form::label('name', 'Name', ['class' => 'col-sm-3 control-label','for'=>'name']) }}
    <div class="col-sm-9">
        {{ Form::text('name', null, ['class' => 'form-control','placeholder' => 'Enter name','id'=>'name', 'required']) }}
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} mb-3">
    {{ Form::label('email', 'Email', ['class' => 'col-sm-3 control-label','for'=>'email']) }}
    <div class="col-sm-9">
        {{ Form::text('email', null, ['class' => 'form-control','placeholder' => 'Enter email','id'=>'email']) }}
        <small class="text-danger">{{ $errors->first(' email') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }} mb-3">
    {{ Form::label('mobile', 'Mobile', ['class' => 'col-sm-3 control-label','for'=>'mobile']) }}
    <div class="col-sm-9">
        {{ Form::text('mobile', null, ['class' => 'form-control','placeholder' => 'Enter mobile','id'=>'mobile', 'required']) }}
        <small class="text-danger">{{ $errors->first('mobile') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} mb-3">
    {{ Form::label('address', 'Address', ['class' => 'col-sm-3 control-label','for'=>'address']) }}
    <div class="col-sm-9">
        {{ Form::text('address', null, ['class' => 'form-control','placeholder' => 'Enter address','id'=>'address', 'required']) }}
        <small class="text-danger">{{ $errors->first('address') }}</small>
    </div>
</div>
