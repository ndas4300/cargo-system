@extends('layouts.master')

@section('title', 'Invoice|Create')


@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0">Create Invoice</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('status.index') }}">Invoice</a></li>
                        <li class="breadcrumb-item active">Create Invoice</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row d-flex justify-content-center">
        <div class="col-12">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="card">

                <div class="invoice p-3 mb-3">

                    <div class="row  d-flex justify-content-between ">
                        <div class="col-3">
                            <h4>
                                <i class="fas fa-globe"></i> Tax Invoice
                            </h4>
                        </div>
                        <div class="col-3">
                            <h4>
                                <small class="float-right">Date: {{now()->format('d-m-Y')}}</small>
                            </h4>

                        </div>

                    </div>
                    {{ Form::open(['method' => 'POST', 'route' => 'invoice.store', 'class' => 'form-horizontal']) }}

                    <div class="row d-flex justify-content-between">
                        <div class="col-sm-4 invoice-col">
                            <address>
                                <div class="form-group{{ $errors->has('supplier') ? ' has-error' : '' }}">
                                    {{ Form::label('supplier', 'Supplier', ['class' => 'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ Form::select('supplier', $suppliers, null, ['class' => 'form-control
                                        select2','placeholder'=>'Select Supplier', 'id' => 'supplier']) }}
                                        <small class="text-danger">{{ $errors->first('supplier') }}</small>
                                    </div>
                                </div>
                                <b>Address:</b>  <div id="supplierAddress"></div>
                                <b>Phone:</b>  <div id="supplierPhone"></div>
                                <b>Email:</b>  <div id="supplierEmail"></div>
                            </address>
                        </div>

                        <div class="col-sm-4 invoice-col">
                            <address>
                                <div class="form-group{{ $errors->has('customer') ? ' has-error' : '' }}">
                                    {{ Form::label('customer', 'Customer', ['class' => 'col-sm-3 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ Form::select('customer', $customers, null, ['class' => 'form-control
                                        select2','placeholder'=>'Select Customer', 'id' => 'customer']) }}
                                        <small class="text-danger">{{ $errors->first('customer') }}</small>
                                    </div>
                                </div>
                                <b>Address:</b>  <div id="customerAddress"></div>
                                <b>Phone:</b>  <div id="customerPhone"></div>
                                <b>Email:</b>  <div id="customerEmail"></div>
                            </address>
                        </div>

                        <div class="col-sm-3 invoice-col">
                            {{-- <b>Invoice #007612</b><br> --}}
                            {{-- <br> --}}
                            {{-- <div class="form-group{{ $errors->has('drop_off_id') ? ' has-error' : '' }}">
                                {{ Form::label('drop_off_id', 'Drop Off Location', ['class' => 'col-sm-12
                                control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('drop_off_id', $drop_offs, null, ['class' => 'form-control
                                    select2','placeholder'=>'Select Drop Off Location', 'id' => 'drop_off_id']) }}
                                    <small class="text-danger">{{ $errors->first('drop_off_id') }}</small>
                                </div>
                            </div> --}}
                            <div class="form-group{{ $errors->has('drop_off_id') ? ' has-error' : '' }}">
                                {{ Form::label('drop_off_id', 'Drop Off Location') }}
                                {{ Form::select('drop_off_id', $drop_offs, null, ['id' => 'drop_off_id', 'class' =>
                                'form-control select2','placeholder'=>'Select Drop Off Location', 'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('drop_off_id') }}</small>
                            </div>
                            <br>
                            {{-- <div class="form-group{{ $errors->has('no_of_box') ? ' has-error' : '' }}">
                                {{ Form::label('no_of_box', 'No. Of Box', ['class' => 'col-sm-12 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::number('no_of_box', null, ['class' => 'form-control', 'required' =>
                                    'required','placeholder'=>'Enter No Of Box']) }}
                                    <small class="text-danger">{{ $errors->first('no_of_box') }}</small>
                                </div>
                            </div> --}}
                            <div class="row mb-1">
                                <div class="col-md-4"><b>No. Of Box:</b></div>
                                <div class="col-md-8">{{ Form::number('no_of_box', null, ['class' => 'form-control',
                                    'required' =>
                                    'required','placeholder'=>'Enter No Of Box']) }}</div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-md-4"><b>Weight:</b></div>
                                <div class="col-md-8">{{ Form::number('weight', null, ['class' => 'form-control',
                                    'required' =>
                                    'required','placeholder'=>'Enter Weight Of Box']) }}</div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-bordered table-hover" id="tab_logic">
                                <thead>
                                    <tr>
                                        <th class="text-center"> # </th>
                                        <th class="text-center"> Product </th>
                                        <th class="text-center"> Qty </th>
                                        <th class="text-center"> Price </th>
                                        <th class="text-center"> Total </th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">
                                    <tr id='addr0'>
                                        <td>1</td>
                                        <td><input type="text" name='product[0]' placeholder='Enter Product Name'
                                                class="form-control" /></td>
                                        <td><input type="number" name='qty[0]' placeholder='Enter Qty'
                                                class="form-control qty" step="0" min="0" /></td>
                                        <td><input type="number" name='price[0]' placeholder='Enter Unit Price'
                                                class="form-control price" step="0.00" min="0" /></td>
                                        <td><input type="number" name='total[0]' placeholder='0.00'
                                                class="form-control total" readonly /></td>
                                    </tr>
                                    {{-- <tr id='addr1'></tr> --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-2">
                            <button id="add_row" type="button" class="btn btn-primary">Add Row</button>
                        </div>
                        <div class="col-md-2">
                            <button id='delete_row' type="button" class="btn btn-danger float-end">Delete Row</button>
                        </div>
                    </div>
                    <div class="row clearfix d-flex justify-content-end" style="margin-top:20px">

                        <div class="pull-right col-md-4">
                            <table class="table table-bordered table-hover" id="tab_logic_total">
                                <tbody>
                                    <tr>
                                        <th class="text-center">Sub Total</th>
                                        <td class="text-center"><input type="number" name='sub_total' placeholder='0.00'
                                                class="form-control" id="sub_total" readonly /></td>
                                    </tr>
                                    <tr>
                                        <th class="text-center">CGST</th>
                                        <td class="text-center">
                                            <div class="input-group mb-2 mb-sm-0">
                                                <input type="number" class="form-control" id="tax1"
                                                    placeholder="0">&nbsp;&nbsp;
                                                <div class="input-group-addon"><i class="fas fa-percent"></i></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-center">CGST Tax Amount</th>
                                        <td class="text-center"><input type="number" name='tax_amount' id="tax_amount"
                                                placeholder='0.00' class="form-control" readonly /></td>
                                    </tr>
                                    <tr>
                                        <th class="text-center">Grand Total</th>
                                        <td class="text-center"><input type="number" name='total_amount'
                                                id="total_amount" placeholder='0.00' class="form-control" readonly />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="row  d-flex">
                        <div class="col-3">
                            {{-- <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i
                                    class="fas fa-print"></i> Print</a> --}}

                        </div>
                        <div class="col-9 d-flex justify-content-end ">
                            {{ Form::button('<i class="far fa-credit-card"></i> Submit Payment', ['class' => 'btn
                            btn-success m-2','type'=>'submit']) }}

                            {{-- <button type="button" class="btn btn-primary m-2">
                                <i class="fas fa-download"></i> Generate PDF
                            </button> --}}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div> <!-- end col -->
    </div>
    <!-- end row -->



</div>
@endsection

@push('script')
<script src="{{ asset('') }}assets/js/pages/form-validation.init.js"></script>

<script>
    $(document).ready(function() {
            var i = 1;
            $("#add_row").click(function() {
                b = i - 1;
                // $('#addr' + i).html($('#addr' + b).html()).find('td:first-child').html(i + 1);
                // $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
                var html = `<tr id='addr`+ i +`'>
                                            <td>`+ (i+1) +`</td>
                                            <td><input type="text" name='product[`+ i +`]' placeholder='Enter Product Name'
                                                    class="form-control" /></td>
                                            <td><input type="number" name='qty[`+ i +`]' placeholder='Enter Qty'
                                                    class="form-control qty" step="0" min="0" /></td>
                                            <td><input type="number" name='price[`+ i +`]' placeholder='Enter Unit Price'
                                                    class="form-control price" step="0.00" min="0" /></td>
                                            <td><input type="number" name='total[`+ i +`]' placeholder='0.00'
                                                    class="form-control total" readonly /></td>
                                        </tr>`;
                $('#tbody').append(html);
                i++;
            });
            $("#delete_row").click(function() {
                if (i > 1) {
                    $("#addr" + (i - 1)).html('');
                    i--;
                }
                calc();
            });

            $('#tab_logic tbody').on('keyup change', function() {
                calc();
            });
            $('#tax1').on('keyup change', function() {
                calc_total();
            });


        });

        function calc() {
            $('#tab_logic tbody tr').each(function(i, element) {
                var html = $(this).html();
                if (html != '') {
                    var qty = $(this).find('.qty').val();
                    var price = $(this).find('.price').val();
                    $(this).find('.total').val(qty * price);

                    calc_total();
                }
            });
        }

        function calc_total() {
            total = 0;
            $('.total').each(function() {
                total += parseInt($(this).val());
            });
            $('#sub_total').val(total.toFixed(2));
            tax_sum = (total / 100) * $('#tax1').val();
            $('#tax_amount').val(tax_sum.toFixed(2));
            $('#total_amount').val((tax_sum + total).toFixed(2));
        }
</script>

<script>
    $('#supplier').change(function() {
            // console.log();
            var id = $(this).val()
            $.ajax({
                url: "{{ route('invoice.getClient') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                },
                success: (function(data) {
                    // console.log(data);
                    $('#supplierAddress').html(data.address);
                    $('#supplierPhone').html(data.mobile);
                    $('#supplierEmail').html(data.email);
                }),
                error: function(error) {
                    console.log(error);
                }
            });
        })

        $('#customer').change(function() {
            // console.log();
            var id = $(this).val()
            $.ajax({
                url: "{{ route('invoice.getClient') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                },
                success: (function(data) {
                    // console.log(data);
                    $('#customerAddress').html(data.address);
                    $('#customerPhone').html(data.mobile);
                    $('#customerEmail').html(data.email);
                }),
                error: function(error) {
                    console.log(error);
                }
            });
        })
</script>
@endpush
@push('style')
    <style>
        .invoice {
            background-color: #fff;
            border: 1px solid rgba(0, 0, 0, .125);
            position: relative;
        }
    </style>
@endpush

