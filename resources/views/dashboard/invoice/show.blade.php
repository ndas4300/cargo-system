@extends('layouts.master')

@section('title', 'Invoice|show')

@section('content')
        <div class="col-12">
            <div class="invoice p-3 mb-3">
                <div class="row d-flex justify-content-between">
                    <div class="col-6">
                        <h4>
                            <i class="fas fa-globe"></i> Tax Invoice
                        </h4>
                    </div>
                    <div class="col-3">
                        <h4>
                            <small class="float-right">Date: {{ $invoice->date }}</small>
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5 invoice-col">
                        Supplier
                        <address>
                            <strong>{{$invoice->supplier->name}}</strong><br>
                            {{$invoice->supplier->address}}<br>
                            Phone: {{$invoice->supplier->mobile}}<br>
                            Email: {{$invoice->supplier->email}}
                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        To
                        <address>
                            <strong>{{$invoice->customer->name}}</strong><br>
                            {{$invoice->customer->address}}<br>
                            Phone: {{$invoice->customer->mobile}}<br>
                            Email: {{$invoice->customer->email}}
                        </address>
                    </div>
                    <div class="col-sm-3 invoice-col">
                        <b>Invoice {{$invoice->invoice_no}}</b><br>
                        <br>
                        <b>Drop Off Loction:</b> {{$invoice->drop_off->name}}<br>
                        <b>No. Off Box:</b> {{$invoice->no_of_box}}<br>
                        <b>Weight:</b> {{$invoice->weight}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th> Serial No </th>
                                    <th> Product </th>
                                    <th> Qty </th>
                                    <th> Price </th>
                                    <th> Total </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($invoice->parcels as $key => $parcel)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{ $parcel->name }}</td>
                                        <td>{{ $parcel->qty }}</td>
                                        <td>{{ $parcel->price }}</td>
                                        <td>{{ $parcel->total }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                    </div>
                    <div class="col-6">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width:50%">Subtotal:</th>
                                        <td>$@php
                                            echo $invoice->grand_total-$invoice->tax
                                        @endphp</td>
                                    </tr>
                                    <tr>
                                        <th>Tax (@php
                                            echo $invoice->grand_total/$invoice->tax
                                        @endphp)%</th>
                                        <td>${{$invoice->tax}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td>${{$invoice->grand_total}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>


                <div class="row no-print d-flex justify-content-between">
                    <div class="col-2">
                        <button class="btn btn-default" id="print"><i
                            class="fas fa-print"></i> Print</button>

                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-success float-right"><i
                                class="far fa-credit-card"></i> Submit
                            Payment
                        </button>
                    </div>
                </div>
            </div>

        </div>
@endsection


@push('script')

<script>
    $('#print').click(function(){
        window.print();
    });
</script>
@endpush

@push('style')
    <style>
        .invoice {
            background-color: #fff;
            border: 1px solid rgba(0, 0, 0, .125);
            position: relative;
        }
    </style>
@endpush
