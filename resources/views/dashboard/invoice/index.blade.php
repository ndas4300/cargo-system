@extends('layouts.master')

@section('title', 'Invoice')


@section('content')
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <h4 class="mb-sm-0">Invoice</h4>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Invoice</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row d-flex justify-content-center">
            <div class="col-12  ">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="d-flex justify-content-end">
                                <a href="{{route('invoice.create')}}" class="btn btn-primary w-md "> Create </a>
                                {{-- <button type="submit" >Submit</button> --}}
                            </div>
                        </div>
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Supplier') }}</th>
                                    <th>{{ __('Customer') }}</th>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Total') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($invoices as $invoice)
                                    <tr>
                                        <td>{{ $invoice->invoice_no }}</td>
                                        <td>{{ $invoice->supplier->name }}</td>
                                        <td>{{ $invoice->customer->name }}</td>
                                        <td>{{ $invoice->date }}</td>
                                        <td>{{ $invoice->grand_total }}</td>
                                        <td nowrap="nowrap">
                                            <a class="btn btn-info" href="{{ route('invoice.show', $invoice->id) }}"><i class="fas fa-eye"></i></a>
                                            {{-- <a class="btn btn-primary" href="{{ route('invoice.edit', $invoice->id) }}"><i class="fas fa-edit"></i></a> --}}
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['invoice.destroy', $invoice->id], 'style' => 'display:inline']) }}
                                            {{ Form::button('<i class="fas fa-trash"  aria-hidden="true"></i>', ['class' => 'btn btn-danger' ,'type'=>'submit']) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
@endsection

@push('style')
    <!-- Responsive datatable examples -->
    <link href="{{ asset('/') }}assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css"
        rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset('/') }}assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('/') }}assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet"
        type="text/css" />
@endpush
@push('script')
    <script src="{{ asset('') }}assets/js/pages/form-validation.init.js"></script>

@endpush
