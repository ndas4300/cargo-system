<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{route('dashboard')}}" class="waves-effect {{ request()->is('dashboard') ? 'mm-active' : null }}">
                        <i class="mdi mdi-home-variant-outline"></i>
                        <span>Dashboard</span>
                    </a>
                </li>


                @can('invoice.access')
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="mdi mdi-file-alert-outline"></i>
                            <span>Invoice</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="{{route('invoice.index')}}">All Invoice</a></li>
                            <li><a href="{{route('invoice.create')}}">Create Invoice</a></li>
                        </ul>
                    </li>
                @endcan
                @can('report.access')
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="mdi mdi-file-alert-outline"></i>
                            <span>Report</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="{{route('report.product')}}">Product</a></li>
                            <li><a href="{{route('report.invoice')}}">Invoice</a></li>
                        </ul>
                    </li>
                @endcan

                @can('product.access')
                <li>
                    <a href="{{route('product.index')}}" class="waves-effect {{ request()->is('product') ? 'mm-active' : null }}">
                        <i class="mdi mdi-account-box-multiple-outline"></i>
                        <span>Product</span>
                    </a>
                </li>
                @endcan
                @can('client.access')
                <li>
                    <a href="{{route('client.index')}}" class="waves-effect {{ request()->is('client') ? 'mm-active' : null }}">
                        <i class="mdi mdi-account-box-multiple-outline"></i>
                        <span>Client</span>
                    </a>
                </li>
                @endcan
                @can('dropOff.access')
                <li>
                    <a href="{{route('dropOff.index')}}" class="waves-effect {{ request()->is('dropOff') ? 'mm-active' : null }}">
                        <i class="mdi mdi-map-marker-check-outline"></i>
                        <span>Drop Off Location</span>
                    </a>
                </li>
                @endcan
                @can('status.access')
                <li>
                    <a href="{{route('status.index')}}" class="waves-effect {{ request()->is('status') ? 'mm-active' : null }}">
                        <i class="mdi mdi-bell"></i>
                        <span>Status</span>
                    </a>
                </li>
                @endcan
                <li class="menu-title">Settings</li>
                @can('user.access')
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect {{ request()->is('user*') ? 'mm-active' : null }}">
                            <i class="mdi mdi-cog-outline"></i>
                            <span>Users</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="{{route('user.index')}}">All User</a></li>
                            <li><a href="{{route('user.create')}}">Create User</a></li>
                            <li><a href="{{route('roles.index')}}">User Roles</a></li>
                        </ul>
                    </li>
                @endcan

                @can('settings.access')
                    <li>
                        <a href="{{ route('company.edit') }}"
                            class="waves-effect {{ request()->is('settings/company-settings') ? 'mm-active' : null }}">
                            <i class="mdi mdi-cog-outline"></i>
                            <span>Company Setting</span>
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
