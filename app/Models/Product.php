<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'destination_info',
        'appx_weight',
        'box',
        'drop_off_id',
        'status_id',
        'comment',
        'date',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function status()
    {
        return $this->belongsTo(Status::class);
    }
    public function dropOff()
    {
        return $this->belongsTo(DropOff::class);
    }
}
