<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_no',
        'no_of_box',
        'weight',
        'drop_off_id',
        'tax',
        'grand_total',
        'supplier_id',
        'customer_id',
        'date',
    ];

    public function drop_off()
    {
        return $this->belongsTo(DropOff::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Client::class,'supplier_id');
    }

    public function customer()
    {
        return $this->belongsTo(Client::class,'customer_id');
    }

    public function parcels()
    {
        return $this->hasMany(Parcel::class);
    }

    public function paymentStatuses()
    {
        return $this->hasMany(PaymentStatus::class);
    }
}
