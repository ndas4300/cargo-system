<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\DropOff;
use App\Models\Parcel;
use App\Models\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->checkPermission('invoice.access');
        $invoices = Invoice::paginate(10);
        $this->putSL($invoices);
        return view('dashboard.invoice.index', compact('invoices'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->checkPermission('invoice.create');
        $suppliers  = Client::pluck('name','id');
        $customers  = Client::pluck('name','id');
        $drop_offs  =  DropOff::pluck('name','id');
        return view('dashboard.invoice.create',compact('suppliers','customers','drop_offs'));
    }

     public function getClient(Request $request)
     {
        $client=Client::findOrFail($request->id);
        return response()->json($client);
     }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('invoice.store');
        // dd($request->all());
        // dd($request->product[0]);
        do {
            $invoice_no = random_int(100000, 999999);
        } while (Invoice::where("invoice_no", "=", $invoice_no)->first());

        $invoice=Invoice::create([
            'invoice_no'       => '#'.$invoice_no,
            'supplier_id'      => $request->supplier,
            'customer_id'      => $request->customer,
            'no_of_box'        => $request->no_of_box,
            'weight'           => $request->weight,
            'drop_off_id'      => $request->drop_off_id,
            'tax'              => $request->tax_amount,
            'grand_total'      => $request->total_amount,
            'date'             => now(),
        ]);

        $count=count($request->product);
        for ($i=0; $i < $count; $i++) {
            Parcel::create([
                'invoice_id'   => $invoice->id,
                'name'         => $request->product[$i],
                'qty'          => $request->qty[$i],
                'price'        => $request->price[$i],
                'total'        => $request->total[$i],

            ]);
        }

        return redirect()->route('invoice.show',$invoice->id)->with('success', 'Invoice Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param Invoice $invoice
     * @return void
     */
    public function show(Invoice $invoice)
    {
        $this->checkPermission('invoice.show');

        return view('dashboard.invoice.show',compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Invoice $invoice
     * @return Response
     */
    public function edit(Invoice $invoice)
    {
        $this->checkPermission('invoice.edit');
        $suppliers  = Client::pluck('name','id');
        $customers  = Client::pluck('name','id');
        $drop_offs  =  DropOff::pluck('name','id');
        return view('dashboard.invoice.edit', compact('invoice','suppliers','customers','drop_offs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Invoice $invoice
     * @return Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $this->checkPermission('invoice.update');
        // dd($request->all());
        $invoice->update([
            'supplier_id'      => $request->supplier,
            'customer_id'      => $request->customer,
            'no_of_box'        => $request->no_of_box,
            'weight'           => $request->weight,
            'drop_off_id'      => $request->drop_off_id,
            'tax'              => $request->tax_amount,
            'grand_total'      => $request->total_amount,
        ]);

        $count=count($request->product);
        for ($i=0; $i < $count; $i++) {
            $parcel=$invoice->parcels;
            $parcel->updateOrCreate([
                'invoice_id'   => $invoice->id,
                'name'         => $request->product[$i],
                'qty'          => $request->qty[$i],
                'price'        => $request->price[$i],
                'total'        => $request->total[$i],

            ]);
        }

        return redirect()->route('invoice.index')->with('success', 'Invoice updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Invoice $invoice
     * @return Response
     */
    public function destroy(Invoice $invoice)
    {
        $this->checkPermission('invoice.destroy');
        $invoice->delete();
        return back()->with('delete', 'Invoice deleted successfully.');
    }

    public function invoice(Invoice $invoice): \Illuminate\Http\RedirectResponse
    {
        if ($invoice->is_active==0) {
            $invoice->is_active ='1';
        }
        else{
            $invoice->is_active ='0';
        }

        $invoice->update();

        return redirect()->route('invoice.index')->with('success', 'Invoice updated successfully.');
    }
}
