<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\DropOff;
use App\Models\Product;
use App\Models\Status;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->checkPermission('product.access');

        $client    =  Client::pluck('name','id');
        $drop_off  =  DropOff::pluck('name','id');
        $status  =  Status::pluck('name','id');
        $products = Product::paginate(10);
        $this->putSL($products);
        return view('dashboard.product.index', compact('products','client','drop_off','status'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    // public function create()
    // {
    //     $this->checkPermission('product.create');
    //     return view('dashboard.product.create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('product.store');
        $validated = $request->validate([
            'client_id'         => ['required', 'integer'],
            'destination_info'  => ['string','nullable'],
            'appx_weight'       => ['integer','required'],
            'box'               => ['integer','required'],
            'drop_off_id'       => ['required', 'integer'],
            'status_id'         => ['integer','nullable'],
            'comment'           => ['string','required'],
            'date'              => ['date','required'],
        ]);

        Product::create($validated);

        return redirect()->back()->with('success', 'Product Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return void
     */
    // public function show(Product $product)
    // {
    //     $this->checkPermission('product.show');
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return Response
     */
    // public function edit(Product $product)
    // {
    //     $this->checkPermission('product.edit');
    //     return view('dashboard.product.edit', compact('product'));
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function update(Request $request, Product $product)
    {
        $this->checkPermission('product.update');
        // dd($request->all());
        $validated = $request->validate([
            'name'          => ['required', 'string', 'max:255'],
            'email'         => ['email','nullable'],
            'address'       => ['string','required'],
            'mobile'        => ['numeric','required'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $product->update($validated);

        return redirect()->back()->with('success', 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        $this->checkPermission('product.destroy');
        $product->delete();
        return back()->with('delete', 'Product deleted successfully.');
    }

    // public function product(Product $product)
    // {
    //     if ($product->is_active==0) {
    //         $product->is_active ='1';
    //     }
    //     else{
    //         $product->is_active ='0';
    //     }

    //     $product->update();

    //     return redirect()->route('product.index')->with('success', 'Product updated successfully.');
    // }
}

