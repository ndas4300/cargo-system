<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->checkPermission('client.access');
        $clients = Client::paginate(10);
        $this->putSL($clients);
        return view('dashboard.client.index', compact('clients'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    // public function create()
    // {
    //     $this->checkPermission('client.create');
    //     return view('dashboard.client.create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('client.store');
        $validated = $request->validate([
            'name'          => ['required', 'string', 'max:255'],
            'email'         => ['email','nullable'],
            'address'       => ['string','required'],
            'mobile'        => ['numeric','required'],
        ]);

        Client::create($validated);

        return redirect()->route('client.index')->with('success', 'Client Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return void
     */
    // public function show(Client $client)
    // {
    //     $this->checkPermission('client.show');
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @return Response
     */
    // public function edit(Client $client)
    // {
    //     $this->checkPermission('client.edit');
    //     return view('dashboard.client.edit', compact('client'));
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Client $client
     * @return Response
     */
    public function update(Request $request, Client $client)
    {
        $this->checkPermission('client.update');
        // dd($request->all());
        $validated = $request->validate([
            'name'          => ['required', 'string', 'max:255'],
            'email'         => ['email','nullable'],
            'address'       => ['string','required'],
            'mobile'        => ['numeric','required'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $client->update($validated);

        return redirect()->route('client.index')->with('success', 'Client updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @return Response
     */
    public function destroy(Client $client)
    {
        $this->checkPermission('client.destroy');
        $client->delete();
        return back()->with('delete', 'Client deleted successfully.');
    }

    // public function client(Client $client)
    // {
    //     if ($client->is_active==0) {
    //         $client->is_active ='1';
    //     }
    //     else{
    //         $client->is_active ='0';
    //     }

    //     $client->update();

    //     return redirect()->route('client.index')->with('success', 'Client updated successfully.');
    // }
}
