<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Status;
use App\Models\DropOff;
use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function product(Request $request)
    {
        $this->checkPermission('report.product');

            if($request->has('from')){
                $products = Product::query()
                ->whereBetween('date',[$request->from,$request->to])
                ->get();
            }else{
                $products = Product::get();
            }

        $client    =  Client::pluck('name','id');
        $drop_off  =  DropOff::pluck('name','id');
        $status  =  Status::pluck('name','id');
        // $this->putSL($products);
        return view('dashboard.report.product', compact('products','client','drop_off','status'));
    }

    public function invoice(Request $request)
    {

        $this->checkPermission('report.invoice');
        if($request->has('from')){
            $products = Invoice::query()
            ->whereBetween('date',[$request->from,$request->to])
            ->get();
        }else{
            $invoices = Invoice::get();
        }
        // $this->putSL($invoices);
        return view('dashboard.report.invoice', compact('invoices'));
    }
}
