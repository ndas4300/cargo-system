<?php

namespace App\Http\Controllers;

use App\Models\DropOff;
use Illuminate\Http\Request;

class DropOffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->checkPermission('dropOff.access');
        $dropOffs = DropOff::get();
        // $this->putSL($dropOffs);
        return view('dashboard.dropOff.index', compact('dropOffs'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->checkPermission('dropOff.create');
        return view('dashboard.dropOff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('dropOff.store');
        $validated = $request->validate([
            'name'          => ['required', 'string','unique:drop_offs', 'max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        DropOff::create($validated);

        return redirect()->route('dropOff.index')->with('success', 'DropOff Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param DropOff $dropOff
     * @return void
     */
    public function show(DropOff $dropOff)
    {
        $this->checkPermission('dropOff.show');
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param DropOff $dropOff
     * @return Response
     */
    public function edit(DropOff $dropOff)
    {
        $this->checkPermission('dropOff.edit');
        return view('dashboard.dropOff.edit', compact('dropOff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param DropOff $dropOff
     * @return Response
     */
    public function update(Request $request, DropOff $dropOff)
    {
        $this->checkPermission('dropOff.update');
        // dd($request->all());
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $dropOff->update($validated);

        return redirect()->route('dropOff.index')->with('success', 'DropOff updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DropOff $dropOff
     * @return Response
     */
    public function destroy(DropOff $dropOff)
    {
        $this->checkPermission('dropOff.destroy');
        $dropOff->delete();
        return back()->with('delete', 'DropOff deleted successfully.');
    }

    public function dropOff(DropOff $dropOff): \Illuminate\Http\RedirectResponse
    {
        if ($dropOff->is_active==0) {
            $dropOff->is_active ='1';
        }
        else{
            $dropOff->is_active ='0';
        }

        $dropOff->update();

        return redirect()->route('dropOff.index')->with('success', 'DropOff updated successfully.');
    }
}
