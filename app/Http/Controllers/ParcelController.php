<?php

namespace App\Http\Controllers;

use App\Models\Parcel;
use Illuminate\Http\Request;

class ParcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->checkPermission('parcel.access');
        $parcels = Parcel::paginate(10);
        $this->putSL($parcels);
        return view('dashboard.parcel.index', compact('parcels'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->checkPermission('parcel.create');
        return view('dashboard.parcel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('parcel.store');
        $validated = $request->validate([
            'name'          => ['required', 'string','unique:drop_offs', 'max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        Parcel::create($validated);

        return redirect()->route('parcel.index')->with('success', 'Parcel Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param Parcel $parcel
     * @return void
     */
    public function show(Parcel $parcel)
    {
        $this->checkPermission('parcel.show');
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Parcel $parcel
     * @return Response
     */
    public function edit(Parcel $parcel)
    {
        $this->checkPermission('parcel.edit');
        return view('dashboard.parcel.edit', compact('parcel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Parcel $parcel
     * @return Response
     */
    public function update(Request $request, Parcel $parcel)
    {
        $this->checkPermission('parcel.update');
        // dd($request->all());
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $parcel->update($validated);

        return redirect()->route('parcel.index')->with('success', 'Parcel updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Parcel $parcel
     * @return Response
     */
    public function destroy(Parcel $parcel)
    {
        $this->checkPermission('parcel.destroy');
        $parcel->delete();
        return back()->with('delete', 'Parcel deleted successfully.');
    }

    public function parcel(Parcel $parcel): \Illuminate\Http\RedirectResponse
    {
        if ($parcel->is_active==0) {
            $parcel->is_active ='1';
        }
        else{
            $parcel->is_active ='0';
        }

        $parcel->update();

        return redirect()->route('parcel.index')->with('success', 'Parcel updated successfully.');
    }
}

